<?PHP


//ajout d'un objet au panier si et seulement si il n'y est pas deja, sinon on change le nombre.
function add_to_basket($prod_name, $prod_price, $prod_quant) {
    if (make_basket() && isset($prod_name)) {
        $prod_pos = array_search($prod_name, $_SESSION['panier']['name']);
        if ($prod_pos !== false)
            $_SESSION['panier']['quant'][$prod_pos] += $prod_quant;
        else {
            array_push($_SESSION['panier']['name'], $prod_name);
            array_push($_SESSION['panier']['quant'], $prod_quant);
            array_push($_SESSION['panier']['price'], $prod_price);
        }
    }
    return TRUE;
}

//initialisation du panier si il n'existe pas
function make_basket() {
    if (!isset($_SESSION['panier'])) {
        $_SESSION['panier'] = array();
        $_SESSION['panier']['name'] = array();
        $_SESSION['panier']['price'] = array();
        $_SESSION['panier']['quant'] = array();
    }
    return TRUE;
}

//changement du nombre d'objets dans le panier, si et seulement si cet objet est deja dans le panier
function modify_quant($prod_name, $prod_quant){
    if (make_basket() && isset($prod_name) && $prod_quant > 0) {
        $prod_pos = array_search($prod_name, $_SESSION['panier']['name']);
        if ($prod_pos !== false) {
            $_SESSION['panier']['quant'][$prod_pos] = $prod_quant;
            return TRUE;
        }
    }
    else if ($prod_quant < 1) {
        remove_from_basket($prod_name);
        return TRUE;
    }
    return FALSE;
}

//retire un objet du panier, peu importe sa quantité
function remove_from_basket($prod_name) {
    if (make_basket() && isset($prod_name)) {
        $prod_pos = array_search($prod_name, $_SESSION['panier']['name']);
        if ($prod_pos !== false) {
            array_splice($_SESSION['panier']['name'], $prod_pos, 1);
            array_splice($_SESSION['panier']['price'], $prod_pos, 1);
            array_splice($_SESSION['panier']['quant'], $prod_pos, 1);
            return TRUE;
        }
    }
    return FALSE;
}