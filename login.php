<?PHP

require_once ('./includes/header.php');
require "users/auth.php";
include './place_order.php';

if (isset($_POST) && isset($_SESSION)) {
	if (isset($_POST['username']) && isset($_POST['password'])) {
		$login = $_POST['username'];
		$passwd = $_POST['password'];
		$passwd = hash('sha512', $passwd);
		if (auth($login, $passwd)) {
			$_SESSION['logged_on_user'] = $login;
			get_cart();
			echo "OK\n";
		}
		else {
			$_SESSION['logged_on_user'] = "";
			echo "ERROR\n";
		}
	}
}
if (($_SESSION['logged_on_user']) === "" || !(isset($_SESSION['logged_on_user']))) {
?>
<form method="POST" action="#"><br/><br/><br/><br/>
  <label for="username">Username : </label>
  <input type="text" placeholder="Username" name="username"/><br>
  <label for="password">Password &nbsp;: </label>
  <input style="width:20%" type="password" placeholder="Password" name="password"/><br><br>
  <input style="width:31%" type="submit" name="submit">
</form>
<?PHP
}
else {
	header("Location: index.php");
	echo "<h2 style='text-align:center;color:red'>Already logged on</h2>";
}
?>