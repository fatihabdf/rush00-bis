<?php
session_start();
?>
<button onclick="location.href='../index.php'">Back to Home</button>
<button onclick="history.go(-1);">Retour a la page precedente</button>

<link rel="stylesheet" href="../style/style.css" type="text/css"/>
<h1>Bienvenue, <?php echo $_SESSION['username']; ?> vous etes connecté </h1>
<br/>
<a href="?action_prod=add">Ajouter un produit</a>
<a href="?action_prod=modifydelete">Modifier/Supprimer un produit</a> <br/><br/>
<a href="?action_cat=add_cat">Ajouter une categorie</a>
<a href="?action_cat=modifydelete_cat">Modifier/Supprimer une categorie</a> <br/><br/>
<a href="?action_user=modifydelete">Modifier/Supprimer un utilisateur</a> <br/><br/>
<?php
try
{
  $db = new PDO('mysql:host=localhost;dbname=rush00', 'root', 'rmerien');
  $db->setAttribute(PDO::ATTR_CASE, PDO::CASE_LOWER);
  $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}
catch(Exception $e) 
{
  echo '<p style="font-size:2vw">Une erreur est survenue</p>';
  die();
}

if(isset($_SESSION['username']))
{
  if(isset($_GET['action_prod']))
  {
    if($_GET['action_prod']=='add')
    {
      if(isset($_POST['submit']))
      {
        $title=$_POST['title'];
        $description=$_POST['description'];
        $price=$_POST['price'];

        $img=$_FILES['img']['name'];
        $img_tmp=$_FILES['img']['tmp_name'];

        if(!empty($img_tmp))
        {
            $image = explode('.', $img);
            $image_ext = end($image);
            if (in_array(strtolower($image_ext), array('png', 'jpg','jpeg')) === false)
                echo "Wrong extension (are accepted .png, .jpg, .jpeg";
            else {
              $image_size = getimagesize($img_tmp);
              if ($image_size['mime'] == 'image/jpeg') {
                $image_src = imagecreatefromjpeg($img_tmp);
              }
              else if ($image_size['mime'] == 'image/png') {
                $image_src = imagecreatefrompng($img_tmp);
              }
              else {
                $image_src = false;
                echo "Please enter valid image file";
              }
              if ($image_src !== false) {
                $image_width = 200;
                if ($image_size[0] == $image_width) {
                  $img_fin = $image_src;
                }
                else {
                  $new_width[0] = $image_width;
                  $new_height[1] = 200;
                  $img_fin = imagecreatetruecolor($new_width[0], $new_height[1]);
                  imagecopyresampled($image_fin, $image_src, 0,0,0,0,$new_width[0],$new_height[1], $image_size[0], $image_size[1]);
                }
                imagejpeg($img_fin, "imgs/".$title.".jpg");
              }
            }
        }
        else
        {
          echo '<p style="font-size:2vw">veuillez rentrer une image</p>';
        }
        if($title&&$description&&$price)
        {
          $category=$_POST['category'];
          $insert = $db->prepare("INSERT INTO products VALUES('','$title', '$description', '$price', '$category')");
          $insert->execute();
          header('Location: admin.php');
        }
        else
        {
          echo '<p style="font-size:2vw">Veuillez remplir tous les champs</p>';
        }
      }
      ?>
     <form action="" method="post" enctype="multipart/form-data">
     <h3>Title du produit</h3><input type="text" name="title"/>
     <h3>Description du produit</h3><textarea type="text" name="description"></textarea>
     <h3>Prix du produit</h3><input type="text" name="price"/>
     <h3>Choisissez la categorie</h3><select name="category">

     <?php  $select=$db->query("SELECT * FROM category");
         while($s = $select->fetch(PDO::FETCH_OBJ))
         {
           ?>
           <option> <?php echo $s->name; ?>  </option>
           <?php
         }
     ?>

     </select>
     <br/><br/>
     <h3>Ajoutez une image</h3><input type="file" name="img"/> <br/><br/>
     <input type="submit" name="submit"> <br/>
     </form>
     <?php
    }
    else if($_GET['action_prod']=='modifydelete')
    {
      $select = $db->prepare("SELECT * FROM products");
      $select->execute();
      while($s=$select->fetch(PDO::FETCH_OBJ)) // tant que ya donne a afficher, les stocker dans $s sous forme d'obj
      {
        echo $s->title;
        ?>
        <a href="?action_prod=modify&amp;id=<?php echo $s->id; ?>">Modifier</a>  <!-- &amp sert a inserer un deuxieme parametre -->
        <a href="?action_prod=delete&amp;id=<?php echo $s->id; ?>">Supprimer</a> <br/>
        <?php
 
      }
    }
    else if($_GET['action_prod']=='modify')
    {
      $id=$_GET['id'];
      $select = $db->prepare("SELECT * FROM products WHERE id=$id");
      $select->execute();

      $data = $select->fetch(PDO::FETCH_OBJ);
      
      ?>

      <form action="" method="post">
      <h3>Title du produit</h3><input value="<?php echo "$data->title;" ?>" type="text" name="title"/>  
      <h3>Description du produit</h3><textarea name="description"> <?php echo $data->description; ?> </textarea>
      <h3>Prix du produit</h3><input value="<?php echo $data->price; ?>" type="text" name="price"/>
      <input type="submit" name="submit" value="modifier"> <br/>
      </form>
      
      <?php

      if(isset($_POST['submit']))
      {
        $title=$_POST['title'];
        $description=$_POST['description'];
        $price=$_POST['price'];

        if($title&&$description&&$price)
        {
          $update = $db->prepare("UPDATE products SET title='$title', description='$description', price='$price' WHERE id=$id");
          $update->execute();
          header('Location: admin.php');
        }
        else
        {
          echo '<p style="font-size:2vw">Veuillez remplir tous les champs</p>';
        }
      }
    }
    else if($_GET['action_prod']=='delete')
    {
      $id=$_GET['id'];
      $delete = $db->prepare("DELETE FROM products WHERE id=$id");
      $delete->execute();
      header('Location: admin.php');
    }
    else
    {
      die('Une erreur s\'est produite');
    }
  }
  else if(isset($_GET['action_cat']))
  {
    if($_GET['action_cat']=='add_cat')
    {
      if(isset($_POST['submit']))
      {
        $name=$_POST['name'];
        if($name)
        {
          $insert = $db->prepare("INSERT INTO category VALUES('','$name')");
          $insert->execute();
          header('Location: admin.php');
        }
        else
        {
          echo '<p style="font-size:2vw">Veuillez remplir le champs</p>';
        }
      }
      ?>
     <form action="" method="post">
     <h3>Nom de la categorie</h3><input type="text" name="name"/>
     <br/><br/>
     <input type="submit" name="submit"> <br/>
     </form>
     <?php
    }
    else if($_GET['action_cat']=='modifydelete_cat')
    {
      $select = $db->prepare("SELECT * FROM category");
      $select->execute();
      while($s=$select->fetch(PDO::FETCH_OBJ)) // tant que ya donne a afficher, les stocker dans $s sous forme d'obj
      {
        echo $s->name;
        ?>
        <a href="?action_cat=modify_cat&amp;id=<?php echo $s->id; ?>">Modifier</a>  <!-- &amp sert a inserer un deuxieme parametre -->
        <a href="?action_cat=delete_cat&amp;id=<?php echo $s->id; ?>">Supprimer</a> <br/>
        <?php
 
      }
    }
    else if($_GET['action_cat']=='modify_cat')
    {
      $id=$_GET['id'];
      $select = $db->prepare("SELECT * FROM category WHERE id=$id");
      $select->execute();

      $data = $select->fetch(PDO::FETCH_OBJ);
      
      ?>

      <form action="" method="post">
      <h3>nom de la categorie</h3><input value="<?php echo "$data->name;" ?>" type="text" name="name"/>  
      <input type="submit" name="submit" value="modifier"> <br/>
      </form>
      
      <?php

      if(isset($_POST['submit']))
      {
        $title=$_POST['name'];

        if($name)
        {
          $update = $db->prepare("UPDATE category SET title='$name' WHERE id=$id");
          $update->execute();
          header('Location: admin.php');
        }
        else
        {
          echo '<p style="font-size:2vw">Veuillez remplir le champ</p>';
        }
      }
    }
    else if($_GET['action_cat']=='delete_cat')
    {
      $id=$_GET['id'];
      $delete = $db->prepare("DELETE FROM category WHERE id=$id");
      $delete->execute();
      header('Location: admin.php');
    }
    else
    {
      die('Une erreur s\'est produite');
    }
  }


  // *********************************************************************************************************************************************************************************
  else if(isset($_GET['action_user']))
  {
    if($_GET['action_user']=='modifydelete')
    { 
      $select = $db->prepare("SELECT * FROM user");
      $select->execute();
      while($s=$select->fetch(PDO::FETCH_OBJ)) // tant que ya donne a afficher, les stocker dans $s sous forme d'obj
      {
        echo $s->login;
        ?>
        <a href="?action_user=modify&amp;id=<?php echo $s->id; ?>">Modifier</a>  <!-- &amp sert a inserer un deuxieme parametre -->
        <a href="?action_user=delete&amp;id=<?php echo $s->id; ?>">Supprimer</a> <br/>
        <?php
      }
    }
    else if($_GET['action_user']=='modify')
    {
      $id=$_GET['id'];
      $select = $db->prepare("SELECT * FROM user WHERE id=$id");
      $select->execute();

      $data = $select->fetch(PDO::FETCH_OBJ);
      
      ?>

      <form action="" method="post">
      <h3>nom de l'utilisateur</h3><input value="<?php echo "$data->login"; ?>" type="text" name="login"/>  
      <input type="submit" name="submit" value="modifier"> <br/>
      </form>
      
      <?php

      if(isset($_POST['submit']))
      {
        $login=$_POST['login'];

        if($login)
        {
          $update = $db->prepare("UPDATE user SET login='$login' WHERE id='$id'");
          $update->execute();
          header('Location: admin.php');
        }
        else
        {
          echo '<p style="font-size:2vw">Veuillez remplir le champ</p>';
        }
      }
    }
    else if($_GET['action_user']=='delete')
    {
      $id=$_GET['id'];
      $delete = $db->prepare("DELETE FROM user WHERE id='$id'");
      $delete->execute();
      header('Location: admin.php?action_user=modifydelete');
    }
    else
    {
      die('Une erreur s\'est produite');
    }
  }
}
else
{
  header('Location: ../index.php');
}
?>