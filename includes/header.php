<?php
session_start();

try
{
  $db = new PDO('mysql:host=localhost;dbname=rush00', 'root', 'rmerien');
  $db->setAttribute(PDO::ATTR_CASE, PDO::CASE_LOWER);
  $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}
catch(Exception $e) 
{
	echo 'Une erreur est survenue';
	die();
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="style/style.css" type="text/css">
  <title>Animalistique</title>
</head>
<body>
<button onclick="location.href='admin/index.php'">admin</button>
  <div id="wrap">
  <div id="head">
  <header>
    <h1>Animalistique</h1>
    <br />
     <ul class="menu" style="padding:0;margin:0;list-style:none">
        <li style="max-width:18%"><a href="index.php">Accueil</a></li>
        <li style="max-width:18%"><a href="boutique.php">Boutique</a></li>
        <li style="max-width:18%"><a href="panier.php">Panier</a></li>
        <?php
        if (!($_SESSION['logged_on_user']))
        {
        echo "<li style=\"max-width:18%\"><a href=\"login.php\">Connexion</a></li>
        <li style=\"max-width:18%\"><a href=\"inscription.php\">Inscription</a></li>";
        }
        else{
          echo "<li style=\"max-width:18%\"><a href=\"logout.php\">Deconnexion</a></li>
          <li style=\"max-width:18%\"><a href=\"change_pw.php\">Modifier PW</a></li>";
        }
        ?>
    </ul>
  </header>
</div>
</div>
</body>
</html>

<?php

?>