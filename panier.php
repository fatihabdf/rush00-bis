<?PHP
include './includes/header.php';
include './cart_func.php';
include './place_order.php';

if (isset($_GET) && $_GET['action'] === 'supression') {
    if (isset($_GET['name'])) {
        remove_from_basket($_GET['name']);
    }
    else
        unset($_SESSION['panier']);
}
if (isset($_GET) && $_GET['submit']) {
    modify_quant($_GET['submit'], $_GET['quantity']);
}
if (isset($_GET) && isset($_GET['place_order'])) {
    if (isset($_SESSION['logged_on_user']) && $_SESSION['logged_on_user'])
        place_order();
    else
        echo "<h2>You need to be logged on to place an order<h2>";
}

?>

<link rel="stylesheet" href="./style/style.css" type="text/css"/>
<html><body>
    <h1 style="text-align:center;font-size:3vw">Votre Panier</h1>
        <table width='82%'>
        <tr>
            <td class="hf">Produit:</td>
            <td class="hf">Prix Unitaire</td>
            <td class="hf">Quantite</td>
            <td class="hf">Prix</td>
        </tr>
        <?php
            $total_price = 0;
            make_basket();
            if (($nb_prod = count($_SESSION['panier']['name'])) < 1)
                echo "<tr><td colspan=4><p style='color:red'>Empty Basket</p></td></tr>";
            else {
                for ($i = 0; $i < $nb_prod; $i++) {
                    ?>
                    <tr>
                        <td><?php echo $_SESSION['panier']['name'][$i]?></td>
                        <td><?php echo $_SESSION['panier']['price'][$i]?>$</td>
                        <form method='get'>
                        <td><input style="width:13%;height:100%;margin:0" type="number" min='1' max='99' id="INeedSleep" name="quantity" value="<?php echo $_SESSION['panier']['quant'][$i]?>"/>
                        <button style="width:84%;height:1vw;margin:0;font-size:0.5vw" type="submit" name="submit" value="<?php echo $_SESSION['panier']['name'][$i]?>">Refresh</button>
                        </form>
                        <td><?php echo ($_SESSION['panier']['quant'][$i]*$_SESSION['panier']['price'][$i])?>$</td>
                        <td class='delete'><a class="del" href="panier.php?action=supression&name=<?PHP echo $_SESSION['panier']['name'][$i]?>">Supprimer</a></td>
                <?php $total_price += ($_SESSION['panier']['quant'][$i]*$_SESSION['panier']['price'][$i]);
                }
            }
            ?></tr>
            <tr>
            <td class="hf" colspan="3">Total :</td>
            <td class="hf"><?php echo $total_price?>$</td>
            <td class="delete"><a class="del" href="panier.php?action=supression">Supprimer Le Panier</a></td>
        </tr>
        </table>
        <form action="#" method='get'>
            <input style="width:10%;margin-left:45%" type='submit' name="place_order" value='Commander'/>
        </form>
</body></html>